/*
El siguiente algoritmo fue solicitado por el kinder “CriCri”, el cuál debe
ayudar en el aprendizaje de las vocales minúsculas y mayúsculas, se debe leer
una letra, el algoritmo debe determinar si es una vocal minúscula o mayúscula,
si es así debe desplegar un mensaje de Felicitación indicando si fue Mayúscula
o minúscula, de lo contrario debe indicarle que no es una vocal. Validar con la
letra A, a

Complementar el algoritmo 6, con las cinco vocales restantes (A,a, E,e, I, i
,O,o, U, u)
 */
package ejercicio6;

import java.util.Scanner;

/**
 *
 * @author juanm
 */
public class Ejercicio6 {

    static Scanner sc = new Scanner(System.in);
    static String ltr;

    public static void main(String[] args) {
        System.out.println("Ingresa una vocal, ingrese 0 para salir");
        ltr = sc.nextLine();
        while (!ltr.equals("0")) {
            if (ltr.length() == 1) {
                if (ltr.equals("a") || ltr.equals("e") || ltr.equals("i") || ltr.equals("o") || ltr.equals("u")) {
                    System.out.println(ltr + " es una minuscula");
                } else if (ltr.equals("A") || ltr.equals("E") || ltr.equals("I") || ltr.equals("O") || ltr.equals("U")) {
                    System.out.println(ltr + " es una mayuscula");
                }else{
                    System.out.println(ltr + " No es una vocal");
                }
            } else {
                System.out.println("Se a ingresado mas de un caracter");
            }
            System.out.println("=====================");
            System.out.println("Ingresa una vocal, ingrese 0 para salir");
            ltr = sc.nextLine();
        }
        System.out.println("Se a ingresado 0");
    }

}
